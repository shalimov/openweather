package ru.pvolan.openweather.activities.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.pvolan.openweather.R
import ru.pvolan.openweather.usecases.main.ForecastItem

class ForecastAdapter : RecyclerView.Adapter<ForecastItemVH>(){

    private var data: List<ForecastItem> = emptyList()


    override fun getItemCount(): Int {
        return data.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastItemVH {
        val inflater : LayoutInflater = parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v = inflater.inflate(R.layout.forecast_item, parent, false);
        return ForecastItemVH(v);
    }


    override fun onBindViewHolder(holder: ForecastItemVH, position: Int) {
        holder.bind(data[position])
    }


    fun setData(data: List<ForecastItem>){
        this.data = data;
        notifyDataSetChanged();
    }

}


class ForecastItemVH(containerView : View) : RecyclerView.ViewHolder(containerView) {

    val textDate : TextView
    val textTemperature : TextView

    init {
        textDate = containerView.findViewById(R.id.textDate);
        textTemperature = containerView.findViewById(R.id.textTemperature);
    }

    fun bind(forecastItem: ForecastItem) {
        textDate.text = forecastItem.dateTime;
        textTemperature.text = forecastItem.temperature;
    }

}
