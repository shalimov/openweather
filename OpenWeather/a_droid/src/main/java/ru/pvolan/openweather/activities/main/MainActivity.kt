package ru.pvolan.openweather.activities.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import ru.pvolan.openweather.R
import ru.pvolan.openweather.usecases.main.ForecastData
import ru.pvolan.uitools.edit_text.EditTextHelper

class MainActivity : AppCompatActivity() {


    private var forecastAdapter : ForecastAdapter? = null
    private var viewModel : MainActivityVM? = null;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.layoutManager = LinearLayoutManager(this)
        forecastAdapter = ForecastAdapter()
        recyclerView.adapter = forecastAdapter;

        editCityName.setOnEditorActionListener({v, actionId, event -> onSearchEditAction()})
        buttonClearCache.setOnClickListener { v -> onClearCacheClick() }

        viewModel = ViewModelProvider(this).get(MainActivityVM::class.java)

        viewModel!!.getErrorText().observe(this, Observer{ t -> onErrorTextChanged(t) })
        viewModel!!.getProgressVisible().observe(this, Observer{ t -> onProgressVisibleChanged(t) })
        viewModel!!.getData().observe(this, Observer{ t -> onDataChanged(t) })

    }



    private fun onProgressVisibleChanged(progressVisible: Boolean?) {
        progressFrame.visibility = if(progressVisible!!) View.VISIBLE else View.GONE;
    }


    private fun onErrorTextChanged(text : String?) {
        textError.visibility = if(text != null) View.VISIBLE else View.GONE;
        textError.text = text;
    }


    private fun onDataChanged(data: ForecastData?) {
        if(data != null) {
            forecastAdapter!!.setData(data.items)
        } else {
            forecastAdapter!!.setData(emptyList())
        }
    }


    private fun onSearchEditAction(): Boolean {
        EditTextHelper.hideKeyboard(editCityName);
        viewModel!!.loadForecast(editCityName.text.toString())
        return true;
    }


    private fun onClearCacheClick() {
        EditTextHelper.hideKeyboard(editCityName);
        viewModel!!.clearCacheAndLoadForecast(editCityName.text.toString())
    }

}
