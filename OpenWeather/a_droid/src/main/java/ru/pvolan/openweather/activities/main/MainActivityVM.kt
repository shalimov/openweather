package ru.pvolan.openweather.activities.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.pvolan.openweather.R
import ru.pvolan.openweather.application.OApp
import ru.pvolan.openweather.usecases.main.ForecastData
import ru.pvolan.tools.string.StringHelper
import ru.pvolan.uitools.live_data.MutableLiveDataCreator

class MainActivityVM : ViewModel() {


    private val progressVisible : MutableLiveData<Boolean> = MutableLiveDataCreator.create(false);
    private val errorText : MutableLiveData<String?> = MutableLiveDataCreator.create(null);
    private val data : MutableLiveData<ForecastData?> = MutableLiveDataCreator.create(null);


    fun getProgressVisible() : LiveData<Boolean>  { return progressVisible }
    fun getErrorText() : LiveData<String?>  { return errorText }
    fun getData() : LiveData<ForecastData?>  { return data }



    fun clearCacheAndLoadForecast(cityName : String)
    {
        if(progressVisible.value!!) return

        OApp.cnt().mainScreenUseCase.clearCache();
        loadForecast(cityName)
    }


    fun loadForecast(cityName : String)
    {
        if(progressVisible.value!!) return

        if(StringHelper.isNullOrEmpty(cityName)){
            errorText.value = OApp.getApp().getString(R.string.main_empty_city_name)
            return
        }

        val forecastObservable = OApp.cnt().mainScreenUseCase.getForecast(cityName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());

        forecastObservable.subscribe(ForecastObserver())
    }


    inner class ForecastObserver : SingleObserver<ForecastData>
    {

        override fun onSubscribe(d: Disposable?) {
            progressVisible.value = true;
        }

        override fun onSuccess(value: ForecastData) {
            progressVisible.value = false;
            errorText.value = null;
            data.value = value;
        }

        override fun onError(e: Throwable?) {
            progressVisible.value = false;
            errorText.value = e?.message ?: "No error message?";
            data.value = null;
        }

    }

}