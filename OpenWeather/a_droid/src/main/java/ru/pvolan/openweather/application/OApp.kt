package ru.pvolan.openweather.application

import android.app.Application
import ru.pvolan.openweather.container.Container
import ru.pvolan.tools.log.ALog

class OApp : Application() {


    companion object {
        private var app : OApp? = null;

        fun getApp() : OApp {
            return app!!
        }

        fun cnt() : Container {
            return app!!.container!!
        }
    }


    private var container : Container? = null;

    override fun onCreate() {
        super.onCreate()

        ALog.log("********************OApp start********************")

        container = Container.create(this);
        app = this;
    }

}