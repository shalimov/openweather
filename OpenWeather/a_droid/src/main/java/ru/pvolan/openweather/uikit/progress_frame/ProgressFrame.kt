package ru.pvolan.openweather.uikit.progress_frame

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar

import ru.pvolan.openweather.R


class ProgressFrame : FrameLayout {

    protected var progressBar: ProgressBar? = null
        private set


    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }


    private fun init() {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


        val v = inflater.inflate(R.layout.indeterminate_progress_bar, this)
        progressBar = findViewById<View>(R.id.progress_bar) as ProgressBar
    }


    override fun onTouchEvent(event: MotionEvent): Boolean {
        return true
    }
}
