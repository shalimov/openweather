package ru.pvolan.openweather.container

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import ru.pvolan.openweather.usecases.main.MainScreenUseCase
import javax.inject.Singleton


@Singleton
@Component
interface Container {


    companion object{
        fun create(appContext: Context) : Container{
            return DaggerContainer.builder().appContext(appContext).build()
        }
    }


    @Component.Builder
    interface Builder {

        @BindsInstance
        fun appContext(appContext: Context): Builder

        fun build(): Container
    }



    val mainScreenUseCase: MainScreenUseCase


}