package ru.pvolan.openweather.usecases._exception

import android.content.Context
import ru.pvolan.openweather.network.R

class UCException(message : String?, inner : Throwable?) : Exception(
    message,
    inner
)

{
    constructor(inner : Throwable) : this (inner.message, inner)
    constructor(message : String) : this (message, null)
}