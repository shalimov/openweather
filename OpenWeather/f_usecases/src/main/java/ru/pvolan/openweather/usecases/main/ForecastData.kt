package ru.pvolan.openweather.usecases.main

data class ForecastData (
    val items : List<ForecastItem>
)

data class ForecastItem(
    val dateTime : String,
    val temperature : String
)