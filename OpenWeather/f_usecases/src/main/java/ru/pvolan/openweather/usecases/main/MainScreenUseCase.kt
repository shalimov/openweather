package ru.pvolan.openweather.usecases.main

import android.content.Context
import io.reactivex.Single
import ru.pvolan.openweather.entities.CityForecast
import ru.pvolan.openweather.logic.forecast.ForecastLogic
import ru.pvolan.openweather.network._exception.ErrorCodeRetrievedException
import ru.pvolan.openweather.usecases._exception.UCException
import ru.pvolan.openweather.usecases.R
import ru.pvolan.tools.calendar.CalendarHelper
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class MainScreenUseCase
@Inject constructor
(
    private val appContext: Context,
    private val forecastLogic: ForecastLogic
)
{


    fun getForecast(cityName : String) : Single<ForecastData> {
        var forecast = forecastLogic.getForecast(cityName)

        forecast = forecast.onErrorResumeNext { e ->
            if(e is ErrorCodeRetrievedException && e.errorCode == 404) {
                Single.error(UCException(appContext.getString(R.string.error_not_found)))
            } else Single.error(UCException(e))
        };

        val res = forecast.map(this::vmMapper);

        return res;
    }


    private fun vmMapper(cityForecast : CityForecast): ForecastData {

        val items = cityForecast.forecast.map { dayForecast ->
            val dateTime = CalendarHelper.format(dayForecast.localDateTime, "dd MMM HH:mm", dayForecast.localDateTime.timeZone)
            val temperature = String.format("%+d\u00B0C", (Math.round(dayForecast.temperature)) )
            return@map ForecastItem(dateTime, temperature)
        }
        return ForecastData(items)

    }


    fun clearCache(){
        forecastLogic.clearCache()
    }
}