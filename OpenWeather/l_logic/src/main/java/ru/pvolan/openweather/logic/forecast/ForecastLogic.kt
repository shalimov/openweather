package ru.pvolan.openweather.logic.forecast

import io.reactivex.Single
import ru.pvolan.openweather.entities.CityForecast
import ru.pvolan.openweather.memstorage.ForecastCacheStorage
import ru.pvolan.openweather.network.forecast.ForecastAPI
import ru.pvolan.tools.log.ALog
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ForecastLogic
@Inject constructor(
    private val forecastCacheStorage: ForecastCacheStorage,
    private val forecastApi: ForecastAPI
)
{


    fun getForecast(cityName : String) : Single<CityForecast> {
        val cached = forecastCacheStorage.get(cityName);

        if(cached != null) return cached;

        var loaded = forecastApi.getForecast(cityName)

        loaded = loaded.map { cityForecast ->
            forecastCacheStorage.put(cityName, cityForecast)
            return@map cityForecast
        };

        return loaded;
    }


    fun clearCache(){
        forecastCacheStorage.clear()
    }
}