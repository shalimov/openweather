package ru.pvolan.openweather.network._exception

import android.content.Context
import org.json.JSONObject
import ru.pvolan.openweather.network.R

class ErrorCodeRetrievedException : NetworkException {


    val errorCode: Int
    val errorMessage: String
    val response: JSONObject


    constructor(
        appContext: Context,
        errorCode: Int,
        errorMessage: String,
        response: JSONObject
    ) : super(appContext.getString(R.string.unexpected_error_code_exception_message, errorCode, errorMessage), null) {


        this.errorCode = errorCode
        this.errorMessage = errorMessage
        this.response = response

    }


}
