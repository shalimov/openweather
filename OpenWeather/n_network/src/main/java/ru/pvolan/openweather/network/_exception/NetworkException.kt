package ru.pvolan.openweather.network._exception

import android.content.Context
import ru.pvolan.openweather.network.R

open class NetworkException(message : String?, inner : Throwable?) : Exception(
    message,
    inner
)
{
    constructor(appContext : Context, inner : Throwable)
     : this (appContext.getString(R.string.network_error, inner.message), inner)
}