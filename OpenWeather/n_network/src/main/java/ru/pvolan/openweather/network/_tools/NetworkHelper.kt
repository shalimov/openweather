package ru.pvolan.openweather.network._tools

import android.content.Context
import io.reactivex.Observable
import io.reactivex.Single
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.pvolan.openweather.network._exception.ErrorCodeRetrievedException
import ru.pvolan.openweather.network._exception.NetworkException
import ru.pvolan.openweather.network.forecast.ForecastConverterFactory
import ru.pvolan.tools.json.JsonHelper
import ru.pvolan.tools.string.StringHelper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkHelper
@Inject constructor (appContext : Context) {

    val appContext : Context
    val retrofit : Retrofit


    init {
        this.appContext = appContext
        retrofit = Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ForecastConverterFactory()) //Maybe move to API class or smth
                //.addConverterFactory(GsonConverterFactory.create()) sic!!!
                .build();
    }




    fun <T> handleResponse(responseObservable: Single<T>): Single<T> {
        val resp = responseObservable.onErrorResumeNext { e: Throwable ->  handleErrorResponse<T>(e)}

        return resp
    }


    private fun <T> handleErrorResponse(e: Throwable): Single<T>? {
        if (e is HttpException) {

            val errorBody = e.response()?.errorBody()?.string();
            if (StringHelper.isNullOrEmpty(errorBody)) {
                return Single.error(NetworkException("Error response empty", null)) //TODO InvalidResponseException
            }

            try {
                val jsonResponse = JSONObject(errorBody);
                val code = JsonHelper.getInt(jsonResponse, "cod")
                val message = JsonHelper.getString(jsonResponse, "message")

                return Single.error(ErrorCodeRetrievedException(appContext, code, message, jsonResponse))

            } catch (e : JSONException){
                return Single.error(NetworkException("Error response invalid: " + e.message + " \nResponse: " + errorBody, e)) //TODO InvalidResponseException
            }

        }

        return Single.error(NetworkException(appContext, e))
    }

}