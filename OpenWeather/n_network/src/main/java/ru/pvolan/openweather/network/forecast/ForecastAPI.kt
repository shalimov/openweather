package ru.pvolan.openweather.network.forecast

import ru.pvolan.openweather.entities.CityForecast
import io.reactivex.Single
import ru.pvolan.openweather.network._tools.ApiKey
import ru.pvolan.openweather.network._tools.NetworkHelper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ForecastAPI
@Inject constructor
(networkHelper: NetworkHelper)
{

    private val apiService: ForecastApiService
    private val networkHelper: NetworkHelper;


    init {
        this.networkHelper = networkHelper
        this.apiService = networkHelper.retrofit.create(ForecastApiService::class.java)
    }



    fun getForecast(cityName : String) : Single<CityForecast> {
        val forecastObservable = apiService.getForecast(cityName, ApiKey.getApiKey(), "metric")

        return networkHelper.handleResponse(forecastObservable);
    }


}