package ru.pvolan.openweather.network.forecast

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import ru.pvolan.openweather.entities.CityForecast

interface ForecastApiService {


    @GET("forecast")
    fun getForecast(
        @Query("q") cityName : String,
        @Query("appid") apiKey : String,
        @Query("units") units : String)
            : Single<CityForecast>

}