package ru.pvolan.openweather.network.forecast

import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Converter
import retrofit2.Retrofit
import ru.pvolan.openweather.entities.City
import ru.pvolan.openweather.entities.CityForecast
import ru.pvolan.openweather.entities.DateTimeForecast
import ru.pvolan.tools.calendar.CalendarHelper
import ru.pvolan.tools.json.JsonHelper
import java.lang.reflect.Type
import java.util.*

class ForecastConverterFactory : Converter.Factory() {

    override fun responseBodyConverter(
        type: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, CityForecast>? {
        if(type == CityForecast::class.java) return ForecastConverter();
        else return null
    }

}

class ForecastConverter : Converter<ResponseBody, CityForecast> {

    override fun convert(value: ResponseBody): CityForecast? {

        val jsonResponse = JSONObject(value.string());
        val jsonCity = JsonHelper.getJsonObject(jsonResponse, "city");
        val jsonCoords = JsonHelper.getJsonObject(jsonCity, "coord");

        val city = City(
            JsonHelper.getString(jsonCity, "name"),
            JsonHelper.getFloat(jsonCoords, "lat"),
            JsonHelper.getFloat(jsonCoords, "lon")
        );

        val timeOffsetSec = JsonHelper.getInt(jsonCity, "timezone");
        val timeZone = createCustomTimeZone(timeOffsetSec);

        val jsonList = JsonHelper.getJsonArray(jsonResponse, "list");
        val forecast = JsonHelper.getListFromJsonArray(jsonList, object : JsonHelper.JsonParser<DateTimeForecast> {
            override fun fromJson(jsonItem: JSONObject): DateTimeForecast {

                val dateUnix = JsonHelper.getLong(jsonItem, "dt");
                val dateCal = CalendarHelper.createFromUnix(dateUnix);
                dateCal.timeZone = timeZone;

                val jsonMain = JsonHelper.getJsonObject(jsonItem, "main");
                val temp = JsonHelper.getFloat(jsonMain, "temp")

                return DateTimeForecast(dateCal, temp)
            }
        })

        return CityForecast(city, forecast)
    }

    private fun createCustomTimeZone(timeOffsetSec: Int): TimeZone {
        val timeOffsetMin = timeOffsetSec / 60;
        val hours = timeOffsetMin / 60;
        val mins = timeOffsetMin % 60;
        val sign = if(timeOffsetMin >=0) '+' else '-'
        val format = String.format("GMT%s%02d%02d", sign, hours, mins)
        return TimeZone.getTimeZone(format)
    }


}
