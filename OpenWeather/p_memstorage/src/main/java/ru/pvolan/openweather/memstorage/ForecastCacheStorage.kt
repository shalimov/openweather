package ru.pvolan.openweather.memstorage

import io.reactivex.Single
import ru.pvolan.openweather.entities.CityForecast
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ForecastCacheStorage
@Inject constructor()
{

    private val cache : MutableMap<String, CityForecast> = HashMap();


    fun put(cityName: String, forecast: CityForecast){
        cache.put(cityName.toLowerCase(), forecast);
    }


    fun get(cityName: String) : Single<CityForecast>? {
        val forecast = cache.get(cityName.toLowerCase()) ?: return null;
        return Single.just(forecast);
    }


    fun clear(){
        cache.clear()
    }

}