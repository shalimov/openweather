package ru.pvolan.openweather.entities

data class City (
    val name: String,
    val lat: Float,
    val lng: Float
)