package ru.pvolan.openweather.entities

data class CityForecast (
    val city: City,
    val forecast: List<DateTimeForecast>
)