package ru.pvolan.openweather.entities

import ru.pvolan.tools.calendar.CalendarHelper
import java.util.*

data class DateTimeForecast (
    val localDateTime: Calendar,
    val temperature: Float
)
{

    override fun toString(): String {
        return "DateTimeForecast(localDateTime=${CalendarHelper.toNiceString(localDateTime)}, temperature=$temperature)"
    }
}