package ru.pvolan.uitools.edit_text

import android.content.Context
import android.os.ResultReceiver
import android.view.View
import android.view.inputmethod.InputMethodManager

object EditTextHelper {

    fun showKeyboard(editText: View) {
        editText.postDelayed({
            editText.requestFocus()
            val imm = editText.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editText, 0)
        }, 100)
    }


    fun showKeyboard(editText: View, resultReceiver: ResultReceiver) {
        editText.postDelayed({
            editText.requestFocus()
            val imm = editText.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editText, 0, resultReceiver)
        }, 100)
    }


    fun hideKeyboard(editText: View) {
        editText.postDelayed({
            val imm = editText.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(editText.windowToken, 0)
        }, 100)
    }


    fun hideKeyboardWithoutPost(editText: View) {
        val imm = editText.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(editText.windowToken, 0)
    }
}
