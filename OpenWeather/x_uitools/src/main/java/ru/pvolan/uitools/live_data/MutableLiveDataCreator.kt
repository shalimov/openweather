package ru.pvolan.uitools.live_data

import androidx.lifecycle.MutableLiveData


object MutableLiveDataCreator {

    fun <T> create(initialValue: T): MutableLiveData<T> {
        val data = MutableLiveData<T>()
        data.value = initialValue
        return data
    }

}
