package ru.pvolan.tools.calendar

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object CalendarHelper {


    val ISOpatternDefault = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    val shortDatePattern = "yyyy-MM-dd"

    fun toNiceString(c: Calendar): String {
        return format(c, "yyyy-MM-dd HH:mm:ss Z", c.timeZone)
    }

    @JvmOverloads
    fun format(c: Calendar, format: String, timeZone: TimeZone = TimeZone.getDefault()): String {
        val simpleDateFormat = SimpleDateFormat(format)
        simpleDateFormat.calendar = c
        simpleDateFormat.timeZone = timeZone
        return simpleDateFormat.format(c.time)
    }


    fun createNewWithZeroTime(source: Calendar): Calendar {
        val newCalendar = source.clone() as Calendar
        newCalendar.set(Calendar.MILLISECOND, 0)
        newCalendar.set(Calendar.SECOND, 0)
        newCalendar.set(Calendar.MINUTE, 0)
        newCalendar.set(Calendar.HOUR_OF_DAY, 0)
        return newCalendar
    }

    fun copyTimeOnly(to: Calendar, from: Calendar) {
        to.set(Calendar.MILLISECOND, 0)
        to.set(Calendar.SECOND, 0)
        to.set(Calendar.MINUTE, 0)
        to.set(Calendar.HOUR_OF_DAY, 0)
        to.set(Calendar.HOUR_OF_DAY, from.get(Calendar.HOUR_OF_DAY))
        to.set(Calendar.MINUTE, from.get(Calendar.MINUTE))
        to.set(Calendar.SECOND, from.get(Calendar.SECOND))
        to.set(Calendar.MILLISECOND, from.get(Calendar.MILLISECOND))
    }


    @Throws(ParseException::class)
    fun parseFormat(str: String, format: String): Calendar {
        val c = Calendar.getInstance()
        val isoFormat = SimpleDateFormat(format)
        c.time = isoFormat.parse(str)!!
        return c
    }

    @JvmOverloads
    fun setTime(to: Calendar, hour: Int, min: Int, sec: Int = 0, ms: Int = 0) {
        to.set(Calendar.MILLISECOND, 0)
        to.set(Calendar.SECOND, 0)
        to.set(Calendar.MINUTE, 0)
        to.set(Calendar.HOUR_OF_DAY, 0)
        to.set(Calendar.HOUR_OF_DAY, hour)
        to.set(Calendar.MINUTE, min)
        to.set(Calendar.SECOND, sec)
        to.set(Calendar.MILLISECOND, ms)
    }

    /**
     * @param month Use constants (Calendar.JANUARY etc)
     */
    @JvmOverloads
    fun create(year: Int, month: Int, day: Int, hour: Int = 0, min: Int = 0, sec: Int = 0, ms: Int = 0): Calendar {
        val c = Calendar.getInstance()
        c.set(Calendar.MILLISECOND, 0)
        c.set(Calendar.SECOND, 0)
        c.set(Calendar.MINUTE, 0)
        c.set(Calendar.HOUR_OF_DAY, 0)
        c.set(Calendar.DAY_OF_MONTH, 1)
        c.set(Calendar.MONTH, Calendar.JANUARY)

        c.set(Calendar.YEAR, year)
        c.set(Calendar.MONTH, month)
        c.set(Calendar.DAY_OF_MONTH, day)
        c.set(Calendar.HOUR_OF_DAY, hour)
        c.set(Calendar.MINUTE, min)
        c.set(Calendar.SECOND, sec)
        c.set(Calendar.MILLISECOND, ms)

        return c
    }


    fun createMSAfterNow(msAfterNow: Int): Calendar {
        val c = Calendar.getInstance()
        c.add(Calendar.MILLISECOND, msAfterNow)
        return c
    }


    fun createHoursAfterNow(hoursAfterNow: Int): Calendar {
        val c = Calendar.getInstance()
        c.add(Calendar.HOUR, hoursAfterNow)
        return c
    }


    fun createFromUnix(unixTimestamp: Long): Calendar {
        val c = Calendar.getInstance()
        c.timeInMillis = (unixTimestamp * 1000)
        return c
    }


    fun toUnixTimestamp(calendar: Calendar): Long {
        return calendar.timeInMillis / 1000
    }

    fun equalsIgnoreTime(d1: Calendar, d2: Calendar): Boolean {
        if (d1.get(Calendar.YEAR) != d2.get(Calendar.YEAR)) return false
        if (d1.get(Calendar.MONTH) != d2.get(Calendar.MONTH)) return false
        return if (d1.get(Calendar.DAY_OF_MONTH) != d2.get(Calendar.DAY_OF_MONTH)) false else true
    }

    //May be not really max
    fun createMaxValue(): Calendar {
        return create(9999, Calendar.DECEMBER, 31, 23, 59, 59, 999)
    }
}