package ru.pvolan.tools.json

import android.util.SparseArray
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.math.BigDecimal


object JsonHelper {


    fun toString(`object`: JSONObject): String {
        return `object`.toString()
    }


    @Throws(JSONException::class)
    fun toJsonObject(jsonString: String?): JSONObject? {
        return if (jsonString == null) null else JSONObject(jsonString)
    }


    @Throws(JSONException::class)
    fun toJsonArray(jsonString: String?): JSONArray? {
        return if (jsonString == null) null else JSONArray(jsonString)
    }


    @Throws(JSONException::class)
    fun getInt(jsonObject: JSONObject, key: String): Int {
        if (jsonObject.isNull(key)) throw JSONException("Value is null for key: $key")
        return jsonObject.getInt(key)
    }


    @Throws(JSONException::class)
    fun getIntOrNull(jsonObject: JSONObject, key: String): Int? {
        return if (jsonObject.isNull(key)) null else jsonObject.optInt(key)
    }


    @Throws(JSONException::class)
    fun getIntOrZero(jsonObject: JSONObject, key: String): Int {
        return if (jsonObject.isNull(key)) 0 else jsonObject.optInt(key)
    }


    @Throws(JSONException::class)
    fun getLong(jsonObject: JSONObject, key: String): Long {
        if (jsonObject.isNull(key)) throw JSONException("Value is null for key: $key")
        return jsonObject.getLong(key)
    }


    @Throws(JSONException::class)
    fun getLongOrNull(jsonObject: JSONObject, key: String): Long? {
        return if (jsonObject.isNull(key)) null else jsonObject.optLong(key)
    }


    @Throws(JSONException::class)
    fun getLongOrZero(jsonObject: JSONObject, key: String): Long {
        return if (jsonObject.isNull(key)) 0 else jsonObject.optLong(key)
    }


    @Throws(JSONException::class)
    fun getObjectOrNull(jsonObject: JSONObject, key: String): Any? {
        return if (jsonObject.isNull(key)) null else jsonObject.get(key)
    }


    @Throws(JSONException::class)
    fun getJsonObjectOrNull(jsonObject: JSONObject, key: String): JSONObject? {
        return if (jsonObject.isNull(key)) null else jsonObject.optJSONObject(key)
    }

    @Throws(JSONException::class)
    fun getJsonObject(jsonObject: JSONObject, key: String): JSONObject {
        if (jsonObject.isNull(key)) throw JSONException("Value is null for key: $key")
        return jsonObject.getJSONObject(key)
    }


    @Throws(JSONException::class)
    fun getStringOrNull(jsonObject: JSONObject, key: String): String? {
        return if (jsonObject.isNull(key)) null else jsonObject.optString(key)
    }

    @Throws(JSONException::class)
    fun getString(jsonObject: JSONObject, key: String): String {
        if (jsonObject.isNull(key)) throw JSONException("Value is null for key: $key")
        return jsonObject.getString(key)
    }


    @Throws(JSONException::class)
    fun getBoolean(jsonObject: JSONObject, key: String): Boolean {
        if (jsonObject.isNull(key)) throw JSONException("Value is null for key: $key")
        return jsonObject.getBoolean(key)
    }


    @Throws(JSONException::class)
    fun getStringOrNullNoEmptyString(jsonObject: JSONObject, key: String): String? {
        if (jsonObject.isNull(key)) {
            return null
        }
        val string = jsonObject.optString(key)
        return if (string == "") null else string
    }


    @Throws(JSONException::class)
    fun getJsonArray(jsonObject: JSONObject, key: String): JSONArray {
        if (jsonObject.isNull(key)) throw JSONException("Value is null for key: $key")
        return jsonObject.getJSONArray(key)
    }

    @Throws(JSONException::class)
    fun getJsonArrayOrEmptyArray(jsonObject: JSONObject, key: String): JSONArray? {
        return if (jsonObject.isNull(key)) JSONArray() else jsonObject.optJSONArray(key)
    }


    @Throws(JSONException::class)
    fun getStringListFromJsonArray(jsonArray: JSONArray): List<String> {
        val strings = ArrayList<String>()
        for (i in 0 until jsonArray.length()) {
            val string = jsonArray.getString(i)
            strings.add(string)
        }

        return strings
    }


    @Throws(JSONException::class)
    fun <T> getListFromJsonArray(jsonArray: JSONArray, parser: JsonParser<T>): List<T> {
        val strings = ArrayList<T>(jsonArray.length())
        for (i in 0 until jsonArray.length()) {
            val `object` = jsonArray.getJSONObject(i)
            strings.add(parser.fromJson(`object`))
        }

        return strings
    }

    interface JsonParser<T> {
        @Throws(JSONException::class)
        fun fromJson(`object`: JSONObject): T
    }


    @Throws(JSONException::class)
    fun <T> listToJsonArray(list: List<T>, generator: JsonGenerator<T>): JSONArray {
        val array = JSONArray()
        for (item in list) {
            array.put(generator.toJson(item))
        }
        return array
    }

    interface JsonGenerator<T> {
        @Throws(JSONException::class)
        fun toJson(`object`: T): JSONObject
    }


    @Throws(JSONException::class)
    fun <T> getSparseArrayFromJsonArray(jsonArray: JSONArray, parser: JsonSparseArrayParser<T>): SparseArray<T> {
        val result = SparseArray<T>(jsonArray.length())
        for (i in 0 until jsonArray.length()) {
            val `object` = jsonArray.getJSONObject(i)
            result.put(parser.keyFromJson(`object`), parser.valueFromJson(`object`))
        }

        return result
    }

    interface JsonSparseArrayParser<T> {
        @Throws(JSONException::class)
        fun valueFromJson(`object`: JSONObject): T

        @Throws(JSONException::class)
        fun keyFromJson(`object`: JSONObject): Int
    }


    @Throws(JSONException::class)
    fun <T> sparseArrayToJsonArray(sparseArray: SparseArray<T>, generator: JsonSparseArrayGenerator<T>): JSONArray {
        val array = JSONArray()
        for (i in 0 until sparseArray.size()) {
            array.put(generator.toJson(sparseArray.keyAt(i), sparseArray.valueAt(i)))
        }
        return array
    }

    interface JsonSparseArrayGenerator<T> {
        @Throws(JSONException::class)
        fun toJson(key: Int, value: T): JSONObject
    }


    @Throws(JSONException::class)
    fun getStringOrEmptyString(jsonObject: JSONObject, key: String): String {
        return if (jsonObject.isNull(key)) "" else jsonObject.optString(key)
    }


    @Throws(JSONException::class)
    fun putObjectOrNull(jsonObject: JSONObject, value: Any?, key: String) {
        val putObject = value ?: JSONObject.NULL
        jsonObject.put(key, putObject)
    }


    @Throws(JSONException::class)
    fun putIntOrNullIfZero(jsonObject: JSONObject, value: Int, key: String) {
        val putObject = if (value != 0) Integer.valueOf(value) else JSONObject.NULL
        jsonObject.put(key, putObject)
    }


    @Throws(JSONException::class)
    fun getBigDecimalOrNull(jsonObject: JSONObject, key: String): BigDecimal? {
        if (jsonObject.isNull(key)) return null
        val valString = jsonObject.optString(key)
        return if (valString == null) null else BigDecimal(valString)
    }


    @Throws(JSONException::class)
    fun getDouble(jsonObject: JSONObject, key: String): Double {
        if (jsonObject.isNull(key)) throw JSONException("Value is null for key: $key")
        return jsonObject.getDouble(key)
    }


    @Throws(JSONException::class)
    fun getDoubleOrNull(jsonObject: JSONObject, key: String): Double? {
        return if (jsonObject.isNull(key)) null else jsonObject.getDouble(key)
    }


    @Throws(JSONException::class)
    fun getFloat(jsonObject: JSONObject, key: String): Float {
        if (jsonObject.isNull(key)) throw JSONException("Value is null for key: $key")
        return jsonObject.getDouble(key).toFloat()
    }


    @Throws(JSONException::class)
    fun getFloatOrNull(jsonObject: JSONObject, key: String): Float? {
        return if (jsonObject.isNull(key)) null else jsonObject.getDouble(key).toFloat()
    }


    @Throws(JSONException::class)
    fun jsonStringArrayToList(value: JSONArray?): List<String>? {
        if (value == null) return null
        val result = ArrayList<String>(value.length())
        for (i in 0 until value.length()) {
            val curItem = value.getString(i)
            result.add(curItem)
        }
        return result
    }


    fun stringListToJsonArray(list: List<String>): JSONArray {
        val array = JSONArray()
        for (str in list) {
            array.put(str)
        }

        return array
    }


    @Throws(JSONException::class)
    fun isExactlyNull(jsonObject: JSONObject, key: String): Boolean {
        if (!jsonObject.has(key)) {
            throw JSONException("No value for key $key")
        }

        return jsonObject.isNull(key)
    }


    fun isNullOrNotPresented(jsonObject: JSONObject, key: String): Boolean {
        return jsonObject.isNull(key)
    }

}
