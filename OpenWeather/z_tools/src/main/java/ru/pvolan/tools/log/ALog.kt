package ru.pvolan.tools.log


import android.util.Log
import ru.pvolan.tools.log.console.AConsole


object ALog {


    private val aConsole = AConsole()


    //***********************************************
    //Publics


    //Basics

    fun log(msg: String) {
        logInternal(msg)
    }

    fun logTagged(tag: String?, msg: String) {
        logInternal(tag, msg)
    }


    fun log(throwable: Throwable?) {
        logInternal(throwable)
    }



    //**************************************
    //Internal


    private fun logInternal(tag: String?, msg: String?) {
        aConsole.consoleLog(tag, msg)
    }


    private fun logInternal(msg: String?) {
        logInternal(null, msg)
    }


    private fun logInternal(throwable: Throwable?) {
        logInternal(createExcString(throwable))
    }



    //**************************************
    //Tools


    private fun createExcString(exc: Throwable?): String? {
        if(exc == null) return null;
        return Log.getStackTraceString(exc)
    }




}
