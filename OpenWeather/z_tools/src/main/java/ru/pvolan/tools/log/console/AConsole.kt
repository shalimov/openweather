package ru.pvolan.tools.log.console

import android.util.Log
import ru.pvolan.tools.string.StringHelper
import java.util.ArrayList

class AConsole {


    fun consoleLog(tag: String?, msg: String?) {
        if (tag != null)
            consoleLog(String.format("[%s] %s", StringHelper.makeOfExactLength(tag, 10), msg))
        else
            consoleLog(msg)
    }

    fun consoleLog(msg: String?) {
        if(msg == null){
            Log.i("ALog", "null")
            return
        }
        val lines = splitTooLongMessages(msg)
        for (line in lines) {
            Log.i("ALog", line)
        }
    }


    private fun splitTooLongMessages(msg: String): List<String> {
        val res = ArrayList<String>()
        val maxLength = 1000
        if (msg.length <= maxLength) {
            res.add(msg)
            return res
        }

        val lines = msg.split("\\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (line in lines) {
            if (line.length <= maxLength) {
                res.add(line)
            } else {
                var index = 0
                while (index < line.length) {
                    res.add(line.substring(index, Math.min(index + maxLength, line.length)))
                    index += maxLength
                }
            }
        }

        return res
    }

}