package ru.pvolan.tools.string


object StringHelper {

    fun makeOfExactLength(str: String, len: Int): String {
        var s: String? = str
        if (s == null) s = ""

        return if (s.length >= len) {
            s.substring(0, len)
        } else {
            String.format("%" + len + "s", s)
        }
    }


    fun isNullOrEmpty(str: String?): Boolean {
        return str?.isEmpty() ?: true
    }

    fun isNullOrEmptyOrWhitespace(str: String?): Boolean {
        return str?.trim { it <= ' ' }?.isEmpty() ?: true
    }

    fun emptyIfNull(str: String?): String {
        return str ?: ""
    }


    fun join(parts: List<String>, delimeter: String): String {
        val b = StringBuilder()
        val lastIndex = parts.size - 1

        for (i in parts.indices) {
            b.append(parts[i])
            if (i != lastIndex) b.append(delimeter)
        }

        return b.toString()
    }


}
